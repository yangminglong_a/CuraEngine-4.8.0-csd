//Copyright (c) 2019 Ultimaker B.V.
//CuraEngine is released under the terms of the AGPLv3 or higher.

#include "Application.h"
#include "FffProcessor.h" //To start a slice.
#include "Scene.h"
#include "sliceDataStorage.h"
#include "Weaver.h"
#include "Wireframe2gcode.h"
#include "communication/Communication.h" //To flush g-code and layer view when we're done.
#include "progress/Progress.h"
#include "utils/logoutput.h"
#include "Slice.h"

// AnyCubic : Hanson
#include "AnyCubic/Roll.h"
#include "AnyCubic/Polygon3D.h"
#include "support.h"

// AnyCubic : Hanson : End


namespace cura
{

Scene::Scene(const size_t num_mesh_groups)
: mesh_groups(num_mesh_groups)
, current_mesh_group(mesh_groups.begin())
{
    for (MeshGroup& mesh_group : mesh_groups)
    {
        mesh_group.settings.setParent(&settings);
    }
}

const std::string Scene::getAllSettingsString() const
{
    std::stringstream output;
    output << settings.getAllSettingsString(); //Global settings.

    //Per-extruder settings.
    for (size_t extruder_nr = 0; extruder_nr < extruders.size(); extruder_nr++)
    {
        output << " -e" << extruder_nr << extruders[extruder_nr].settings.getAllSettingsString();
    }

    for (size_t mesh_group_index = 0; mesh_group_index < mesh_groups.size(); mesh_group_index++)
    {
        if (mesh_group_index == 0)
        {
            output << " -g";
        }
        else
        {
            output << " --next";
        }

        //Per-mesh-group settings.
        const MeshGroup& mesh_group = mesh_groups[mesh_group_index];
        output << mesh_group.settings.getAllSettingsString();

        //Per-object settings.
        for (size_t mesh_index = 0; mesh_index < mesh_group.meshes.size(); mesh_index++)
        {
            const Mesh& mesh = mesh_group.meshes[mesh_index];
            output << " -e" << mesh.settings.get<size_t>("extruder_nr") << " -l \"" << mesh_index << "\"" << mesh.settings.getAllSettingsString();
        }
    }
    output << "\n";

    return output.str();
}

void Scene::processMeshGroup(MeshGroup& mesh_group)
{
    FffProcessor* fff_processor = FffProcessor::getInstance();
    fff_processor->time_keeper.restart();

    TimeKeeper time_keeper_total;
	

	 
    bool empty = true;
    for (Mesh& mesh : mesh_group.meshes)
    {
        if (!mesh.settings.get<bool>("infill_mesh") && !mesh.settings.get<bool>("anti_overhang_mesh"))
        {
            empty = false;
            break;
        }
    }

    if (empty)
    {
        Progress::messageProgress(Progress::Stage::FINISH, 1, 1); // 100% on this meshgroup
        log("Total time elapsed %5.2fs.\n", time_keeper_total.restart());
        return;
    }
	// wireframe 模式
    if (mesh_group.settings.get<bool>("wireframe_enabled"))
    {
        log("Starting Neith Weaver...\n");

        Weaver weaver;
        weaver.weave(&mesh_group);
        
        log("Starting Neith Gcode generation...\n");
        Wireframe2gcode gcoder(weaver, fff_processor->gcode_writer.gcode);
        gcoder.writeGCode();
        log("Finished Neith Gcode generation...\n");
    }
    else //Normal operation (not wireframe). 普通模式
    {
		// ANYCUBIC : Hanson
		// cura 参数设置调用
		log("Starting AnyCubic Gcode generation...\n");
		Settings& mesh_group_settings = Application::getInstance().current_slice->scene.current_mesh_group->settings;
		// 首层层厚
		coord_t height_0 = mesh_group_settings.get<coord_t>("layer_height_0");
		// 层厚
		coord_t layer_height = mesh_group_settings.get<coord_t>("layer_height");
		
		bool support_enable_his = mesh_group_settings.get<bool>("support_enable");

		coord_t support_line_width = mesh_group_settings.get<coord_t>("support_line_width");

		const AngleRadians support_angle = mesh_group_settings.get<AngleRadians>("support_angle");
		const double tan_angle = tan(support_angle) - 0.01;  //The X/Y component of the support angle. 0.01 to make 90 degrees work too.
		const coord_t max_dist_from_lower_layer = tan_angle * layer_height; //Maximum horizontal distance that can be bridged.

		const size_t wall_count = mesh_group_settings.get<size_t>("support_wall_count");

		Line3D lineA, lineB;

		// 获取 mesh_group 的 AABB
		AABB3D rawMeshAABB = getMeshsBound(mesh_group.meshes);

		QVector3D supportDir(0, 0, 1);

		SliceDataStorage storage;

		// 创建旋转量
		QQuaternion q = QQuaternion::fromEulerAngles(QVector3D(-45, 0, 0));
		QQuaternion qr = QQuaternion::fromEulerAngles(QVector3D(45, 0, 0));

		coord_t maximum_resolution = std::numeric_limits<coord_t>::max();
		coord_t maximum_deviation = std::numeric_limits<coord_t>::max();

		if (false == support_enable_his) {
			// 将 mesh_group 以一个整体绕X轴顺时针旋转 45 度 ( 与倾斜角度相同 )
			doMeshRotate(mesh_group.meshes, q);
			// 将旋转后的模型以一个整体移动到平台,遍历模型数据,获取真实包围盒,依据包围盒设置Z偏移量
			doMeshToPlat(mesh_group.meshes, rawMeshAABB);

			if (!fff_processor->polygon_generator.generateAreas(storage, &mesh_group, fff_processor->time_keeper, true)) {
				log("Starting AnyCubic Gcode generation :  return : error\n");
				return;
			}
		} else 
		{
			log("Starting AnyCubic Gcode generation : \n");

			/*  *************  旋转、平移模型  ************* */

			// 处理模型：旋转，然后平移到平台，在后续步骤中计算出路径后再偏移Y值与Z值即可得到


			// 包围盒水平放大
			coord_t exLen = (rawMeshAABB.max.x - rawMeshAABB.min.x)+(rawMeshAABB.max.y - rawMeshAABB.min.y)+(rawMeshAABB.max.z - rawMeshAABB.min.z);
			AABB3D rawAABB3DEx = rawMeshAABB;
			rawAABB3DEx.expandXY(exLen);
			// 用于创建裁剪轮廓，在后续步骤中裁剪掉打印平台以下的支撑
			lineA.ptA = Point3(rawAABB3DEx.min.x, rawAABB3DEx.min.y, 0 /*height_0*/);
			lineA.ptB = Point3(rawAABB3DEx.min.x, rawAABB3DEx.max.y, 0 /*height_0*/);
			lineB.ptA = Point3(rawAABB3DEx.max.x, rawAABB3DEx.min.y, 0 /*height_0*/);
			lineB.ptB = Point3(rawAABB3DEx.max.x, rawAABB3DEx.max.y, 0 /*height_0*/);

			// 将 mesh_group 以一个整体绕X轴顺时针旋转 -45 度 ( 与倾斜角度相同 )
			doMeshRotate(mesh_group.meshes, q,  &lineA, &lineB);
			//// 将旋转后的模型以一个整体移动到平台,遍历模型数据,获取真实包围盒,依据包围盒设置Z偏移量
			doMeshToPlat(mesh_group.meshes, rawMeshAABB, &lineA, &lineB);

			mesh_group.updateAABB();

			log("Starting AnyCubic Gcode generation : 2d to 3d finished\n");


			// ************* 对旋转模型切片 *************
			if (!fff_processor->polygon_generator.generateAreas(storage, &mesh_group, fff_processor->time_keeper, true)) {
				log("Starting AnyCubic Gcode generation :  return : error\n");
				return;
			}

			//************ 从上往下遍历模型层轮廓，将每一层轮廓进行反向旋转（相比上一次），从2D轮廓转为3D轮廓，移除Z量，得到只有XY的平面有效区域， *************
			// 遍历模型,取出每一层模型轮廓数据
			std::map<coord_t, std::vector<SliceLayerPart*>> layerPolygons;
			std::vector<coord_t> zHeights; // 
			for (SliceMeshStorage& meshStorage : storage.meshes) {
				// 遍历模型的每一层
				for (SliceLayer& layer : meshStorage.layers) {
					std::map<coord_t, std::vector<SliceLayerPart*>>::iterator it = layerPolygons.find(layer.printZ);
					if (it == layerPolygons.end()) {
						zHeights.push_back(layer.printZ);
						layerPolygons[layer.printZ] = std::vector<SliceLayerPart*>();
						it = layerPolygons.find(layer.printZ);
					}
					for (SliceLayerPart& part : layer.parts) {
						it->second.push_back(&part);
					}
				}
			}

			// 旋转
			auto  doRotatePolygons = [](const QQuaternion& q, Polygons& datas, coord_t zHeight) {

				ClipperLib::Paths::iterator itPaths = datas.begin();

				for (; itPaths != datas.end(); ++itPaths) {
					ClipperLib::Path::iterator itPath = itPaths->begin();
					for (; itPath != itPaths->end(); ++itPath) {
						ClipperLib::IntPoint& pt = *itPath;
						QVector3D pt3d = q.rotatedVector(QVector3D(pt.X, pt.Y, zHeight));
						pt.X = pt3d.x();
						pt.Y = pt3d.y();
					}
				}
				return datas;
			};

			auto  doRotatePolygons3D = [](const QQuaternion& q, Polygons& datas, const Polygon3Ds& data3Ds) {
				
				ClipperLib::Paths::iterator itPaths = datas.begin();
				Polygon3Ds::const_iterator itP3ds = data3Ds.begin();
				for (; itPaths != datas.end(); ++itPaths, ++itP3ds) {
					ClipperLib::Path::iterator itPath = itPaths->begin();
					Polygon3D::const_iterator itP3d = itP3ds->begin();
					for (; itPath != itPaths->end(); ++itPath, ++itP3d) {
						ClipperLib::IntPoint& pt = *itPath;
						QVector3D pt3d = q.rotatedVector( *itP3d );
						pt.X = pt3d.x();
						pt.Y = pt3d.y();
					}
				}
				return datas;
			};

			QVector3D lineDirA = lineA.ptA - lineA.ptB;
			QVector3D lineDirB = lineB.ptA - lineB.ptB;
			// 创建用于裁剪的正方形轮廓，避免生成的支撑在平台之下。
			std::function<Polygons(coord_t)> generateDiffPolygon = [lineA, lineB, lineDirA, lineDirB, exLen](coord_t zheight) {

				QVector3D planeOri(0, 0, zheight);  // C
				QVector3D planeDir(0, 0, 1);        // N

				Plane plane(planeOri, planeDir);

				QVector3D ptIntersectA;
				QVector3D ptIntersectB; 

				if (plane.intersectionRay(ptIntersectA, lineA.ptB, lineDirA) == false ) {
					log("Starting AnyCubic Gcode generation : look like an error \n");
					return Polygons();
				}

				if (plane.intersectionRay(ptIntersectB, lineB.ptB, lineDirB) == false ) {
					log("Starting AnyCubic Gcode generation : look like an error \n");
					return Polygons();
				}

				QVector3D ptIntersectExA = ptIntersectA + QVector3D(0, -1, 0) * exLen*100;
				QVector3D ptIntersectExB = ptIntersectB + QVector3D(0, -1, 0) * exLen * 100;

				Polygon polygon;

				// 首尾不重复
				polygon.add(Point(ptIntersectA.x(), ptIntersectA.y()));
				polygon.add(Point(ptIntersectExA.x(), ptIntersectExA.y()));
				polygon.add(Point(ptIntersectExB.x(), ptIntersectExB.y()));
				polygon.add(Point(ptIntersectB.x(), ptIntersectB.y()));

				Polygons polygons;
				polygons.add(polygon);

				return polygons;
			};


			// 将支撑轮廓（倾斜）按照其平面射线（or反向射线）方向，求其在层高上的交点
			std::function<Point3(const Plane& , coord_t , const Point&)> processProjection = [](const Plane& plane, coord_t minZpos, const Point& pt2d) {

				QVector3D rayOri(pt2d.X, pt2d.Y, minZpos);
				QVector3D rayDir(0, 0, 1);

				QVector3D ptIntersect;
				if (false == plane.intersectionRay(ptIntersect, rayOri, rayDir)) {
					log("Starting AnyCubic Gcode generation : look like an error \n");
				}

				return ptIntersect.toPoint3();
			};

			auto  doProjectionPolygons = [processProjection](Polygon3Ds& polygon3ds, const Polygons& data, const Plane& plane, coord_t minZpos) {

				polygon3ds.resize(data.size());
				Polygon3Ds::iterator itPoly3ds = polygon3ds.begin();

				ClipperLib::Paths::const_iterator itPaths = data.begin();

				for (; itPaths != data.end(); ++itPaths, ++itPoly3ds) {
					itPoly3ds->resize(itPaths->size());
					Polygon3D::iterator itPoly3d = itPoly3ds->begin();
					ClipperLib::Path::const_iterator itPath = itPaths->begin();

					for (; itPath != itPaths->end(); ++itPath, ++itPoly3d) {
						const ClipperLib::IntPoint& pt = *itPath;
						*itPoly3d = processProjection(plane, minZpos, pt);
					}
				}
				return data;
			};

			Polygons uplayerMeshPart_rotateBack;
			Polygons downlayerMeshPart_rotateBack;

			std::vector<Polygons> layerSupports(zHeights.size());
			std::vector<Polygon3Ds> layerSupport3Ds(zHeights.size());
			std::vector<Polygons> layerSolids(zHeights.size());

			const ESupportType support_type = mesh_group_settings.get<ESupportType>("support_type");
			if (support_type == ESupportType::PLATFORM_ONLY) {
				for (int curLayerIndex = 0; curLayerIndex < zHeights.size(); ++curLayerIndex) {
					std::map<coord_t, std::vector<SliceLayerPart*>>::iterator layerParts = layerPolygons.find(zHeights[curLayerIndex]);
					// 这一层的模型轮廓
					Polygons layerMeshPart;
					if (layerParts != layerPolygons.end()) {
						for (SliceLayerPart* part : layerParts->second) {
							layerMeshPart = layerMeshPart.unionPolygons(part->outline);
						}
					}
					// 放大模型轮廓，用于避免支撑与模型接触
					Polygons layerMeshPartOffset = layerMeshPart.offset(max_dist_from_lower_layer);
					// 旋转回原始模型摆放，计算支撑有效区间轮廓，放大后的大小
					Polygons& layerMeshPartOffset_rotateBack = doRotatePolygons(qr, layerMeshPartOffset, zHeights[curLayerIndex]);
					// 从下往上，
					layerSolids[curLayerIndex] = layerMeshPartOffset_rotateBack.unionPolygons(downlayerMeshPart_rotateBack);

					downlayerMeshPart_rotateBack = layerSolids[curLayerIndex];
				}
			}


			for (int curLayerIndex = zHeights.size() - 2; true ; --curLayerIndex) {
				int curValidLayerIndex = curLayerIndex >= 0 ? curLayerIndex : 0;
				
				if (curLayerIndex < 0) {
					layerSupports.insert(layerSupports.begin(), Polygons());
					layerSupport3Ds.insert(layerSupport3Ds.begin(), Polygon3Ds());
					zHeights.insert(zHeights.begin(), height_0 + curLayerIndex*layer_height);
					//layerPolygons.insert(std::pair<coord_t, std::vector<SliceLayerPart*>>(zHeights[0], std::vector<SliceLayerPart*>()));
				}

				// 这一层的模型轮廓
				std::map<coord_t, std::vector<SliceLayerPart*>>::iterator layerParts = layerPolygons.find(zHeights[curValidLayerIndex]);
				Polygons layerMeshPart;
				if (layerParts != layerPolygons.end()) {
					for (SliceLayerPart* part : layerParts->second) {
						layerMeshPart = layerMeshPart.unionPolygons(part->outline);
					}
				}

				// 放大模型轮廓，用于避免支撑与模型接触
				Polygons layerMeshPartOffset = layerMeshPart.offset(max_dist_from_lower_layer);

				// 旋转回原始模型摆放，计算支撑有效区间轮廓，一个原始大小，一个放大后的大小
				Polygons& layerMeshPart_rotateBack = doRotatePolygons(qr, layerMeshPart, zHeights[curValidLayerIndex]);
				Polygons& layerMeshPartOffset_rotateBack = doRotatePolygons(qr, layerMeshPartOffset, zHeights[curValidLayerIndex]);
				// 上一层有效区域-本层有效区域（放大以产生支撑与模型的间隙） = 本层有效支撑区域
				layerSupports[curValidLayerIndex] = uplayerMeshPart_rotateBack.difference(layerMeshPartOffset_rotateBack).difference(layerSolids[curValidLayerIndex]);
				// 
				uplayerMeshPart_rotateBack = layerSupports[curValidLayerIndex].unionPolygons(layerMeshPart_rotateBack);

				uplayerMeshPart_rotateBack = uplayerMeshPart_rotateBack.offset(MM2INT(1), ClipperLib::jtSquare);
				uplayerMeshPart_rotateBack = uplayerMeshPart_rotateBack.offset(MM2INT(-1));

				QVector3D planeO (0, 0, zHeights[curValidLayerIndex]);
				QVector3D planeN (0, 0, 1);
				planeO = qr.rotatedVector(planeO);
				planeN = qr.rotatedVector(planeN).normalized();
				// 将本层有效支撑区域基于向量（原点，方向）投影到平面，重新产生Z值
				doProjectionPolygons(layerSupport3Ds[curValidLayerIndex], layerSupports[curValidLayerIndex], Plane(planeO, planeN), lineA.ptB.z);
				// 将每一层支撑轮廓进行旋转，转为最终打印的2D平面，
				doRotatePolygons3D(q, layerSupports[curValidLayerIndex], layerSupport3Ds[curValidLayerIndex]);

				// 裁剪打印平面以下的支撑轮廓
				Polygons cuttPolygons = generateDiffPolygon(zHeights[curValidLayerIndex]);
				layerSupports[curValidLayerIndex] = layerSupports[curValidLayerIndex].difference(cuttPolygons);
				

				if (curLayerIndex < 0 ) {
					layerSupports[curValidLayerIndex].simplify();

					if (layerSupports[curValidLayerIndex].size() == 0) {
						zHeights.erase(zHeights.begin());
						break;
					}

					if (!storage.support.supportLayers[storage.support.layer_nr_max_filled_layer].isEmpty())
						storage.support.layer_nr_max_filled_layer++;
					storage.support.supportLayers.insert(storage.support.supportLayers.begin(), SupportLayer());
					storage.print_layer_count++;
					for (SliceMeshStorage& st : storage.meshes) {
						SliceLayer layer;
						layer.printZ = zHeights[curValidLayerIndex];
						layer.thickness = layer_height;

						st.layers.insert(st.layers.begin(), layer);
						st.layer_nr_max_filled_layer++;
					}
				}

				// 清空原有支撑轮廓
				storage.support.supportLayers[curValidLayerIndex].clear();
				
				// 将支撑轮廓保存到容器
				std::vector<SupportInfillPart>& support_infill_parts = storage.support.supportLayers[curValidLayerIndex].support_infill_parts;
				std::vector<PolygonsPart> parts = layerSupports[curValidLayerIndex].splitIntoParts(true);
				for (PolygonsPart& part : parts) {
					support_infill_parts.push_back(SupportInfillPart(part, support_line_width, wall_count));
					support_infill_parts.back().generateInsetsAndInfillAreas();

					support_infill_parts.back().infill_area_per_combine_per_density.push_back(std::vector<Polygons>());
					support_infill_parts.back().infill_area_per_combine_per_density.back().push_back(support_infill_parts.back().outline);
				}

				if (storage.support.layer_nr_max_filled_layer == 0 && !storage.support.supportLayers[curValidLayerIndex].isEmpty()) {
					storage.support.layer_nr_max_filled_layer = curValidLayerIndex;
				}
			}

			storage.support.generated = true;

			coord_t zoffset = height_0 - zHeights[0];
			// 遍历模型, 重设层高
			for (SliceMeshStorage& meshStorage : storage.meshes) {
				// 遍历模型的每一层
				for (SliceLayer& layer : meshStorage.layers) {
					layer.printZ = layer.printZ + zoffset;
				}
			}

		}
		// ANYCUBIC : Hanson :END
        
        Progress::messageProgressStage(Progress::Stage::EXPORT, &fff_processor->time_keeper);
        fff_processor->gcode_writer.writeGCode(storage, fff_processor->time_keeper);
    }

    Progress::messageProgress(Progress::Stage::FINISH, 1, 1); // 100% on this meshgroup
    Application::getInstance().communication->flushGCode();
    Application::getInstance().communication->sendOptimizedLayerData();
    log("Total time elapsed %5.2fs.\n", time_keeper_total.restart());
}

} //namespace cura