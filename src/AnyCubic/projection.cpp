#include "projection.h"
#include "../sliceDataStorage.h"
#include "../AnyCubic/Roll.h"
#include "polygon3d.h"

namespace cura {

	// 将3D轮廓投影到水平面成为2D轮廓
	// 将每个水平面上的所有2D轮廓进行布尔与运算

	void calcProjection(SliceDataStorage& storage
		, const std::vector<coord_t> zHeights
		, const std::vector<std::vector<SupportLayer3D*>> indexMap
		, const QVector3D& supportDir)
	{
		QVector3D planeN(0, 0, 1);


		std::vector<coord_t>::const_iterator itHeight= zHeights.begin(); 
		std::vector<std::vector<SupportLayer3D*>>::const_iterator itMap = indexMap.begin();
		for (; itMap != indexMap.end(); ++itMap, ++itHeight) {
			coord_t height = *itHeight;
			QVector3D planeO( 0, 0, height);

			
		}
	}
}
