#include "Roll.h"
#include "../sliceDataStorage.h"
#include "../Application.h"
#include "../Slice.h"

namespace cura
{




	QVector3D QVector3D::normalized() const
	{
		// Need some extra precision if the length is very small.
		double len = double(v[0]) * double(v[0]) +
			double(v[1]) * double(v[1]) +
			double(v[2]) * double(v[2]);
		if (qFuzzyIsNull(len - 1.0f)) {
			return *this;
		}
		else if (!qFuzzyIsNull(len)) {
			double sqrtLen = std::sqrt(len);
			return QVector3D(float(double(v[0]) / sqrtLen),
				float(double(v[1]) / sqrtLen),
				float(double(v[2]) / sqrtLen));
		}
		else {
			return QVector3D();
		}
	}

	/*!
	Normalizes the currect vector in place.  Nothing happens if this
	vector is a null vector or the length of the vector is very close to 1.

	\sa length(), normalized()
	*/
	void QVector3D::normalize()
	{
		// Need some extra precision if the length is very small.
		double len = double(v[0]) * double(v[0]) +
			double(v[1]) * double(v[1]) +
			double(v[2]) * double(v[2]);
		if (qFuzzyIsNull(len - 1.0f) || qFuzzyIsNull(len))
			return;

		len = std::sqrt(len);

		v[0] = float(double(v[0]) / len);
		v[1] = float(double(v[1]) / len);
		v[2] = float(double(v[2]) / len);
	}

	float QVector3D::lengthSquared() const
	{
		return v[0] * v[0] + v[1] * v[1] + v[2] * v[2];
	}

	MeshVertex QVector3D::toMeshVertex() const
	{
		return MeshVertex(toPoint3());
	}

	Point3 QVector3D::toPoint3() const
	{
		return Point3(v[0], v[1], v[2]);
	}

	float QVector3D::dotProduct(const QVector3D& v1, const QVector3D& v2)
	{
		return v1.v[0] * v2.v[0] + v1.v[1] * v2.v[1] + v1.v[2] * v2.v[2];
	}

	/*!
	Returns the cross-product of vectors \a v1 and \a v2, which corresponds
	to the normal vector of a plane defined by \a v1 and \a v2.

	\sa normal()
	*/
	QVector3D QVector3D::crossProduct(const QVector3D& v1, const QVector3D& v2)
	{
		return QVector3D(v1.v[1] * v2.v[2] - v1.v[2] * v2.v[1],
			v1.v[2] * v2.v[0] - v1.v[0] * v2.v[2],
			v1.v[0] * v2.v[1] - v1.v[1] * v2.v[0]);
	}

	QQuaternion QQuaternion::rotationTo(const QVector3D &from, const QVector3D &to)
	{
		// Based on Stan Melax's article in Game Programming Gems

		const QVector3D v0(from.normalized());
		const QVector3D v1(to.normalized());

		float d = QVector3D::dotProduct(v0, v1) + 1.0f;

		// if dest vector is close to the inverse of source vector, ANY axis of rotation is valid
		if (qFuzzyIsNull(d)) {
			QVector3D axis = QVector3D::crossProduct(QVector3D(1.0f, 0.0f, 0.0f), v0);
			if (qFuzzyIsNull(axis.lengthSquared()))
				axis = QVector3D::crossProduct(QVector3D(0.0f, 1.0f, 0.0f), v0);
			axis.normalize();

			// same as QQuaternion::fromAxisAndAngle(axis, 180.0f)
			return QQuaternion(0.0f, axis.x(), axis.y(), axis.z());
		}

		d = std::sqrt(2.0f * d);
		const QVector3D axis(QVector3D::crossProduct(v0, v1) / d);

		return QQuaternion(d * 0.5f, axis).normalized();
	}


	QVector3D QQuaternion::rotatedVector(const QVector3D& vector) const
	{
		return (*this * QQuaternion(0, vector) * conjugated()).vector();
	}

	QQuaternion QQuaternion::normalized() const
	{
		// Need some extra precision if the length is very small.
		double len = double(xp) * double(xp) +
			double(yp) * double(yp) +
			double(zp) * double(zp) +
			double(wp) * double(wp);
		if (qFuzzyIsNull(len - 1.0f))
			return *this;
		else if (!qFuzzyIsNull(len))
			return *this / std::sqrt(len);
		else
			return QQuaternion(0.0f, 0.0f, 0.0f, 0.0f);
	}

	/*!
	Normalizes the current quaternion in place.  Nothing happens if this
	is a null quaternion or the length of the quaternion is very close to 1.

	\sa length(), normalized()
	*/
	void QQuaternion::normalize()
	{
		// Need some extra precision if the length is very small.
		double len = double(xp) * double(xp) +
			double(yp) * double(yp) +
			double(zp) * double(zp) +
			double(wp) * double(wp);
		if (qFuzzyIsNull(len - 1.0f) || qFuzzyIsNull(len))
			return;

		len = std::sqrt(len);

		xp /= len;
		yp /= len;
		zp /= len;
		wp /= len;
	}

	QQuaternion QQuaternion::fromEulerAngles(float pitch, float yaw, float roll)
	{
		// Algorithm from:
		// http://www.j3d.org/matrix_faq/matrfaq_latest.html#Q60

		pitch = qDegreesToRadians(pitch);
		yaw = qDegreesToRadians(yaw);
		roll = qDegreesToRadians(roll);

		pitch *= 0.5f;
		yaw *= 0.5f;
		roll *= 0.5f;

		const float c1 = std::cos(yaw);
		const float s1 = std::sin(yaw);
		const float c2 = std::cos(roll);
		const float s2 = std::sin(roll);
		const float c3 = std::cos(pitch);
		const float s3 = std::sin(pitch);
		const float c1c2 = c1 * c2;
		const float s1s2 = s1 * s2;

		const float w = c1c2 * c3 + s1s2 * s3;
		const float x = c1c2 * s3 + s1s2 * c3;
		const float y = s1 * c2 * c3 - c1 * s2 * s3;
		const float z = c1 * s2 * c3 - s1 * c2 * s3;

		return QQuaternion(w, x, y, z);
	}

	AABB3D getMeshBound(const Mesh& mesh)
	{
		AABB3D aabb;

		for (const MeshVertex& v : mesh.vertices) {
			aabb.include(v.p);
		}
		return aabb;
	}

	AABB3D getMeshsBound(const std::vector<Mesh>& meshs)
	{
		AABB3D aabb;

		for (const Mesh& mesh : meshs) {
			AABB3D meshAabb = getMeshBound(mesh);
			aabb.include(meshAabb);
		}
		return aabb; 
	}

	void doUpdatePolygons(Polygons& polygons, std::function<void(Point&) > callback) {

	}

	void doMeshRotate(std::vector<Mesh>& meshs, const QQuaternion& q, Line3D* lineA, Line3D* lineB)
	{
		// 依据旋转量
		for (Mesh& mesh : meshs) {
			for (MeshVertex& v : mesh.vertices) {
				v = q.rotatedVector(QVector3D(v.p.x, v.p.y, v.p.z)).toMeshVertex();
			}
		}


		// 线段旋转，包围盒上底面Ymin-Ymax方向的两条线段
		if (nullptr != lineA && nullptr != lineB) {
			lineA->ptA = q.rotatedVector(lineA->ptA).toPoint3();
			lineA->ptB = q.rotatedVector(lineA->ptB).toPoint3();
			lineB->ptA = q.rotatedVector(lineB->ptA).toPoint3();
			lineB->ptB = q.rotatedVector(lineB->ptB).toPoint3();
		}
	}



	void doMeshToPlat(std::vector<Mesh>& meshs, AABB3D preAabb, Line3D* lineA, Line3D* lineB)
	{
		// 计算模型包围盒
		AABB3D curAabb = getMeshsBound(meshs);

		Point3 middle = preAabb.getMiddle();

		// 移动模型到平台且XY位置还原
		Point3 offsets(middle.x - curAabb.getMiddle().x, middle.y-curAabb.getMiddle().y, 0 - curAabb.min.z);
		// 模型移动到平台
		for (Mesh& mesh : meshs) {
			mesh.offset(offsets);
		}

		if (nullptr != lineA && nullptr != lineB) {
			lineA->ptA += offsets;
			lineA->ptB += offsets;
			lineB->ptA += offsets;
			lineB->ptB += offsets;
		}
	}


	//QVector3D calcRayHitPlane(const QVector3D& rayO, const QVector3D& rayDir, const QVector3D& planeO, const QVector3D& planeDir) 
	//{
	//	QVector3D T = rayO - planeO;
	//	float k = -QVector3D::dotProduct(T, planeDir) / QVector3D::dotProduct(rayDir, planeDir);
	//	return rayO + rayDir*k;
	//}

}
