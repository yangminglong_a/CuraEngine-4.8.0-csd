#include "Polygon3D.h"
#include "../utils/polygon.h"

namespace cura {



SupportStorage3D::SupportStorage3D()
	: generated(false)
	, layer_nr_max_filled_layer(-1)
{

}


void Polygon3D::traverse(std::function<void(Point3&)> callback)
{
	m_aabb = AABB3D();
	for (std::vector<Point3>::iterator it = begin(); it != end(); ++it)
	{
		callback(*it);
		m_aabb.include(*it);
	}
}

void Polygon3D::fromPolygon2D(const ClipperLib::Path& data, int nLayer, std::function<void(int, const Point&, Point3&)> callback)
{
	m_aabb = AABB3D();

	this->resize(data.size());

	std::vector<Point3>::iterator it3D = this->begin();
	ClipperLib::Path::const_iterator it2D = data.begin();

	for (; it3D != this->end(); ++it3D, ++it2D) {
		callback(nLayer, *it2D, *it3D);
		m_aabb.include(*it3D);
	}
}



Polygon& Polygon2DFromPolygon3D(Polygon& polygon2d, const Polygon3D& polygon3d, coord_t zHeight, std::function<Point(coord_t, const Point3&)> callback) {
	polygon2d.reserve(polygon3d.size());

	for (const Point3& pt3 : polygon3d) {
		polygon2d.add(callback(zHeight, pt3));
	}

	return polygon2d;
}


void Polygon3Ds::traverse(std::function<void(Point3&)> callback)
{
	m_aabb = AABB3D();
	for (std::vector<Polygon3D>::iterator it = begin(); it != end(); ++it)
	{
		it->traverse(callback);
		m_aabb.include(it->aabb());
	}
}

void Polygon3Ds::fromPolygon2Ds(const Polygons& data, int nLayer, std::function<void(int, const Point&, Point3&)> callback)
{
	m_aabb = AABB3D();
	this->resize(data.size());

	std::vector<Polygon3D>::iterator it3D = this->begin();
	ClipperLib::Paths::const_iterator it2D = data.begin();

	for (; it3D != this->end(); ++it3D, ++it2D) {
		it3D->fromPolygon2D(*it2D, nLayer, callback); 
		m_aabb.include(it3D->aabb());
	}
}

PolygonsPart& Polygon2DsFromPolygon3Ds(PolygonsPart& polygon2ds, const Polygon3Ds& polygon3ds, coord_t zHeight, std::function<Point(coord_t, const Point3&)> callback) {

	for (const Polygon3D& polygon3d : polygon3ds) {
		if (polygon3d.aabb().min.z > zHeight || polygon3d.aabb().max.z < zHeight) {
			continue;
		}
		Polygon polygon;
		polygon2ds.add(Polygon2DFromPolygon3D(polygon, polygon3d, zHeight, callback));
	}

	return polygon2ds;
}
/*
Polygon3Ds outline;  //!< The outline of the support infill area
std::vector<Polygon3Ds> insets;  //!< The insets are also known as perimeters or the walls.
AABB3D outline_boundary_box;  //!< The boundary box for the infill area
coord_t support_line_width;  //!< The support line width
int inset_count_to_generate;  //!< The number of insets need to be generated from the outline. This is not the actual insets that will be generated.
std::vector<std::vector<Polygon3Ds>> infill_area_per_combine_per_density;  //!< a list of separated sub-areas which requires different infill densities and combined thicknesses


Polygon3Ds infill_area;  //!< The support infill area for generating patterns

*/
void SupportInfillPart3D::traverse(std::function<void(Point3&)> callback)
{
	m_aabb = AABB3D();

	outline.traverse(callback);
	m_aabb.include(outline.aabb());
	//for (std::vector<Polygon3Ds>::iterator it = insets.begin(); it != insets.end(); ++it) {
	//	it->traverse(callback);
	//	m_aabb.include(it->aabb());
	//}

	for (std::vector<std::vector<Polygon3Ds>>::iterator itPer = infill_area_per_combine_per_density.begin(); itPer != infill_area_per_combine_per_density.end(); ++itPer) {
		for (std::vector<Polygon3Ds>::iterator it = itPer->begin(); it != itPer->end(); ++it) {
			it->traverse(callback);
			m_aabb.include(it->aabb());
		}
	}

	//infill_area.traverse(callback);
	//m_aabb.include(infill_area.aabb());
}

void SupportInfillPart3D::fromSupportInfillPart2D(const SupportInfillPart& data, int nLayer, std::function<void(int, const Point&, Point3&)> callback)
{
	m_aabb = AABB3D();

	this-> support_line_width = data.support_line_width;  //!< The support line width
	this-> inset_count_to_generate = data.inset_count_to_generate;  //!< The number of insets need to be generated from the outline. This is not the actual insets that will be generated.
	
	this->outline.fromPolygon2Ds(data.outline, nLayer, callback);
	
	m_aabb.include(this->outline.aabb());

	//this->infill_area.fromPolygon2Ds(data.getInfillArea(), nLayer, callback);
	//


	//std::vector<Polygon3Ds>::iterator it3D = this->insets.begin();
	//std::vector<Polygons>::const_iterator it2D = data.insets.begin();
	//for (; it3D != this->insets.end(); ++it3D, ++it2D) {
	//	it3D->fromPolygon2Ds(*it2D, nLayer, callback);
	//	m_aabb.include(it3D->aabb());
	//}

	std::vector<std::vector<Polygon3Ds>>::iterator itP3D = this->infill_area_per_combine_per_density.begin();
	std::vector<std::vector<Polygons>>::const_iterator itP2D = data.infill_area_per_combine_per_density.begin();

	for (; itP3D != this->infill_area_per_combine_per_density.end(); ++itP3D, ++itP2D) {
		std::vector<Polygon3Ds>::iterator it3D = itP3D->begin();
		std::vector<Polygons>::const_iterator it2D = itP2D->begin();

		for (; it3D != itP3D->end(); ++it3D, ++it2D) {
			it3D->fromPolygon2Ds(*it2D, nLayer, callback);
			m_aabb.include(it3D->aabb());
		}
	}

}
//
//PolygonsPart SupportInfillPart2DFromSupportInfillPart3D(const SupportInfillPart3D& supportInfillPart3d, coord_t zHeight, std::function<Point(coord_t, const Point3&)> callback) {
//	PolygonsPart polygon2ds;
//	return Polygon2DsFromPolygon3Ds(polygon2ds, supportInfillPart3d.outline, zHeight, callback);
//}


/*
std::vector<SupportInfillPart3D> support_infill_parts;  //!< a list of support infill parts
Polygon3Ds support_bottom; //!< Piece of support below the support and above the model. This must not overlap with any of the support_infill_parts or support_roof.
Polygon3Ds support_roof; //!< Piece of support above the support and below the model. This must not overlap with any of the support_infill_parts or support_bottom.
Polygon3Ds support_mesh_drop_down; //!< Areas from support meshes which should be supported by more support
Polygon3Ds support_mesh; //!< Areas from support meshes which should NOT be supported by more support
Polygon3Ds anti_overhang; //!< Areas where no overhang should be detected.
*/

void SupportLayer3D::traverse(std::function<void(Point3&)> callback)
{
	m_aabb = AABB3D();
	for (std::vector<SupportInfillPart3D>::iterator it = support_infill_parts.begin(); it != support_infill_parts.end(); ++it) {
		it->traverse(callback);
		m_aabb.include(it->aabb());
	}

	support_bottom.traverse(callback);
	support_roof.traverse(callback);
	support_mesh_drop_down.traverse(callback);
	support_mesh.traverse(callback);
	anti_overhang.traverse(callback);

	m_aabb.include(support_bottom.aabb());
	m_aabb.include(support_roof.aabb());
	m_aabb.include(support_mesh_drop_down.aabb());
	m_aabb.include(support_mesh.aabb());
	m_aabb.include(anti_overhang.aabb());
}

/*
std::vector<SupportInfillPart3D>  support_infill_parts;  //!< a list of support infill parts
Polygon3Ds support_bottom;          //!< Piece of support below the support and above the model. This must not overlap with any of the support_infill_parts or support_roof.
Polygon3Ds support_roof;           //!< Piece of support above the support and below the model. This must not overlap with any of the support_infill_parts or support_bottom.
Polygon3Ds support_mesh_drop_down; //!< Areas from support meshes which should be supported by more support
Polygon3Ds support_mesh;           //!< Areas from support meshes which should NOT be supported by more support
Polygon3Ds anti_overhang;          //!< Areas where no overhang should be detected.
*/
void SupportLayer3D::fromSupportLayer2D(const SupportLayer& data, int nLayer, std::function<void(int, const Point&, Point3&)> callback)
{
	m_aabb = AABB3D();

	support_infill_parts  .clear();
	support_infill_parts  .resize(data.support_infill_parts  .size());

	support_bottom        .fromPolygon2Ds(data.support_bottom        , nLayer, callback);
	support_roof          .fromPolygon2Ds(data.support_roof          , nLayer, callback);
	support_mesh_drop_down.fromPolygon2Ds(data.support_mesh_drop_down, nLayer, callback);
	support_mesh          .fromPolygon2Ds(data.support_mesh          , nLayer, callback);
	anti_overhang         .fromPolygon2Ds(data.anti_overhang         , nLayer, callback);

	m_aabb.include(support_bottom.aabb());
	m_aabb.include(support_roof.aabb());
	m_aabb.include(support_mesh_drop_down.aabb());
	m_aabb.include(support_mesh.aabb());
	m_aabb.include(anti_overhang.aabb());

	std::vector<SupportInfillPart3D>::iterator     it3D = this->support_infill_parts.begin();
	std::vector<SupportInfillPart>::const_iterator it2D = data.support_infill_parts.begin();

	for (; it3D != this->support_infill_parts.end(); ++it3D, ++it2D) {
		it3D->fromSupportInfillPart2D(*it2D, nLayer, callback);
		m_aabb.include(it3D->aabb());
	}
}

SupportLayer& SupportLayer2DFromSupportLayer3DList(SupportLayer& supportLayer2d, const std::vector<SupportLayer3D*>& supportLayer3DList, coord_t zHeight, std::function<Point(coord_t, const Point3&)> callback, std::function<Polygons(coord_t)> generatePolygon, std::map<coord_t, std::vector<SliceLayerPart*>>& layerPolygons) {

	supportLayer2d.clear();
	// 取一层多个模型轮廓
	std::map<coord_t, std::vector<SliceLayerPart*>>::iterator itLayer = layerPolygons.find(zHeight);
	// 有模型轮廓
	bool meshPolygonsExist = itLayer != layerPolygons.end();
	// 用于避免支撑与模型接触
	PolygonsPart layerMeshPart;
	if (meshPolygonsExist) {
		for (SliceLayerPart* part : itLayer->second) {
			layerMeshPart = layerMeshPart.unionPolygons(part->outline);
		}
		layerMeshPart = layerMeshPart.offset(MM2INT(0.2));
	}
	// 避免生成的支撑在平台之下。
	Polygons diffPolygons = generatePolygon(zHeight);

	PolygonsPart supportInfillPart_outline;
	coord_t support_line_width = -1;  //!< The support line width
	int inset_count_to_generate = -1;  //!< The number of insets need to be generated from the outline. This is not the actual insets that will be generated.
	for (const SupportLayer3D* supportLayer3d : supportLayer3DList) {
		PolygonsPart support_bottom         ; Polygon2DsFromPolygon3Ds(support_bottom        , supportLayer3d->support_bottom, zHeight, callback);
		PolygonsPart support_roof           ; Polygon2DsFromPolygon3Ds(support_roof          , supportLayer3d->support_roof, zHeight, callback);
		PolygonsPart support_mesh_drop_down ; Polygon2DsFromPolygon3Ds(support_mesh_drop_down, supportLayer3d->support_mesh_drop_down, zHeight, callback);
		PolygonsPart support_mesh           ; Polygon2DsFromPolygon3Ds(support_mesh          , supportLayer3d->support_mesh, zHeight, callback);
		PolygonsPart anti_overhang          ; Polygon2DsFromPolygon3Ds(anti_overhang         , supportLayer3d->anti_overhang, zHeight, callback);

		if (!support_bottom        .empty()) { supportLayer2d.support_bottom         = supportLayer2d.support_bottom        .difference(layerMeshPart); supportLayer2d.support_bottom         = supportLayer2d.support_bottom        .unionPolygons(support_bottom        );	}
		if (!support_roof          .empty()) { supportLayer2d.support_roof           = supportLayer2d.support_roof          .difference(layerMeshPart); supportLayer2d.support_roof           = supportLayer2d.support_roof          .unionPolygons(support_roof          );	}
		if (!support_mesh_drop_down.empty()) { supportLayer2d.support_mesh_drop_down = supportLayer2d.support_mesh_drop_down.difference(layerMeshPart); supportLayer2d.support_mesh_drop_down = supportLayer2d.support_mesh_drop_down.unionPolygons(support_mesh_drop_down);	}
		if (!support_mesh          .empty()) { supportLayer2d.support_mesh           = supportLayer2d.support_mesh          .difference(layerMeshPart); supportLayer2d.support_mesh           = supportLayer2d.support_mesh          .unionPolygons(support_mesh          );	}
		if (!anti_overhang         .empty()) { supportLayer2d.anti_overhang          = supportLayer2d.anti_overhang         .difference(layerMeshPart); supportLayer2d.anti_overhang          = supportLayer2d.anti_overhang         .unionPolygons(anti_overhang         );	}

		for (const SupportInfillPart3D& supportInfillPart3d : supportLayer3d->support_infill_parts) {
			PolygonsPart polygon2ds;
			Polygon2DsFromPolygon3Ds(polygon2ds, supportInfillPart3d.outline, zHeight, callback);
			if (!polygon2ds.empty()) {
				supportInfillPart_outline = supportInfillPart_outline.unionPolygons(polygon2ds);
				// 会重复设置
				support_line_width = supportInfillPart3d.support_line_width;
				inset_count_to_generate = supportInfillPart3d.inset_count_to_generate;
			}
		} 
	}

	if (!supportInfillPart_outline.empty()) {
		// 用于避免支撑与模型接触
		supportInfillPart_outline = supportInfillPart_outline.difference(layerMeshPart);
		// 避免生成的支撑在平台之下。
		supportInfillPart_outline = supportInfillPart_outline.difference(diffPolygons);
		supportInfillPart_outline = supportInfillPart_outline.unionPolygons();

		if (!supportInfillPart_outline.empty()) {
			supportLayer2d.support_infill_parts.push_back(SupportInfillPart(supportInfillPart_outline, support_line_width, inset_count_to_generate));
			supportLayer2d.support_infill_parts.back().generateInsetsAndInfillAreas();

			supportLayer2d.support_infill_parts.back().infill_area_per_combine_per_density.push_back(std::vector<Polygons>());
			supportLayer2d.support_infill_parts.back().infill_area_per_combine_per_density.back().push_back(supportLayer2d.support_infill_parts.back().outline);
		}
	}

	//supportLayer2d.support_bottom.unionPolygons();
	//supportLayer2d.support_roof.unionPolygons();
	//supportLayer2d.support_mesh_drop_down.unionPolygons();
	//supportLayer2d.support_mesh.unionPolygons();
	//supportLayer2d.anti_overhang.unionPolygons();

	supportLayer2d.support_bottom = supportLayer2d.support_bottom.difference(diffPolygons);
	supportLayer2d.support_roof = supportLayer2d.support_roof.difference(diffPolygons);
	supportLayer2d.support_mesh_drop_down = supportLayer2d.support_mesh_drop_down.difference(diffPolygons);
	supportLayer2d.support_mesh = supportLayer2d.support_mesh.difference(diffPolygons);
	supportLayer2d.anti_overhang = supportLayer2d.anti_overhang.difference(diffPolygons);

	return supportLayer2d;
}
/*
std::vector<SupportLayer3D> supportLayers;
*/
void SupportStorage3D::traverse(std::function<void(Point3&)> callback)
{
	m_aabb = AABB3D();
	for (std::vector<SupportLayer3D>::iterator it = supportLayers.begin(); it != supportLayers.end(); ++it) {
		it->traverse(callback);
		m_aabb.include(it->aabb());
	}
}
/*
bool generated; //!< whether generateSupportGrid(.) has completed (successfully)

int layer_nr_max_filled_layer; //!< the layer number of the uppermost layer with content

std::vector<AngleDegrees> support_infill_angles; //!< a list of angle values which is cycled through to determine the infill angle of each layer
std::vector<AngleDegrees> support_infill_angles_layer_0; //!< a list of angle values which is cycled through to determine the infill angle of each layer
std::vector<AngleDegrees> support_roof_angles; //!< a list of angle values which is cycled through to determine the infill angle of each layer
std::vector<AngleDegrees> support_bottom_angles; //!< a list of angle values which is cycled through to determine the infill angle of each layer

std::vector<SupportLayer3D> supportLayers;
*/
void SupportStorage3D::fromSupportStorage2D(const SupportStorage& data, std::function<void(int, const Point&, Point3&)> callback)
{
	m_aabb = AABB3D();

	this->generated = data.generated;

	this->layer_nr_max_filled_layer = data.layer_nr_max_filled_layer;
	this->support_infill_angles = data.support_infill_angles;
	this->support_infill_angles_layer_0 = data.support_infill_angles_layer_0;
	this->support_roof_angles = data.support_roof_angles;
	this->support_bottom_angles = data.support_bottom_angles;

	this->supportLayers.clear();
	this->supportLayers.resize(data.supportLayers.size());

	std::vector<SupportLayer3D>::iterator     it3D = this->supportLayers.begin();
	std::vector<SupportLayer>::const_iterator it2D = data.supportLayers.begin();

	for (int nLayer = 0; it3D != this->supportLayers.end(); ++it3D, ++it2D, ++nLayer) {
		it3D->fromSupportLayer2D(*it2D, nLayer, callback);

		m_aabb.include(it3D->aabb());
	}
}


SupportStorage& SupportStorage2DFromSupportStorage3D(SupportStorage& supportStorage2d, const SupportStorage3D& supportStorage3d, const std::vector<coord_t>& zHeights, const std::vector<std::vector<SupportLayer3D*>>& indexMap, std::function<Point(coord_t, const Point3&)> callback, std::function<Polygons(coord_t)> generatePolygon, std::map<coord_t, std::vector<SliceLayerPart*>>& layerPolygons) {

	supportStorage2d.generated = supportStorage3d.generated;
	supportStorage2d.layer_nr_max_filled_layer = supportStorage3d.layer_nr_max_filled_layer;

	std::vector<coord_t>::const_iterator itZ = zHeights.begin();
	std::vector<std::vector<SupportLayer3D*>>::const_iterator itMap = indexMap.begin();

	supportStorage2d.supportLayers.clear();
	for (; itZ != zHeights.end(); ++itZ, ++itMap) {
		SupportLayer supportLayer2d;
		supportStorage2d.supportLayers.push_back(SupportLayer2DFromSupportLayer3DList(supportLayer2d, *itMap, *itZ, callback, generatePolygon, layerPolygons));
	}

	supportStorage2d.support_infill_angles.clear();
	supportStorage2d.support_infill_angles_layer_0.clear();
	supportStorage2d.support_roof_angles.clear();
	supportStorage2d.support_bottom_angles.clear();
	// 重设 AngleDegrees
	for (int i = 0; i < zHeights.size(); ++i) {
		if (supportStorage3d.support_infill_angles.size()) supportStorage2d.support_infill_angles.push_back(supportStorage3d.support_infill_angles.at(i%supportStorage3d.support_infill_angles.size()));
		if (supportStorage3d.support_infill_angles_layer_0.size()) supportStorage2d.support_infill_angles_layer_0.push_back(supportStorage3d.support_infill_angles_layer_0.at(i%supportStorage3d.support_infill_angles_layer_0.size()));
		if (supportStorage3d.support_roof_angles.size()) supportStorage2d.support_roof_angles.push_back(supportStorage3d.support_roof_angles.at(i%supportStorage3d.support_roof_angles.size()));
		if (supportStorage3d.support_bottom_angles.size()) supportStorage2d.support_bottom_angles.push_back(supportStorage3d.support_bottom_angles.at(i%supportStorage3d.support_bottom_angles.size()));
	}

	for (int i = zHeights.size() - 1; i >= 0; --i) {
		//去除无效支撑
		if (i == zHeights.size() - 1) {
			supportStorage2d.supportLayers.at(i).clear();
			continue;
		}

		if (supportStorage2d.supportLayers.at(i).isEmpty()) {
			continue;
		} else {
			supportStorage2d.layer_nr_max_filled_layer = i;
			break;
		}
	}

	return supportStorage2d;
}

}
