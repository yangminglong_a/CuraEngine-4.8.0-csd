// AnyCubic : Hanson

#ifndef ANYCUBIC_ROLL_H
#define ANYCUBIC_ROLL_H

#include "../mesh.h"
#include "Polygon3D.h"

namespace cura
{
	static inline  bool qFuzzyIsNull(float f)
	{
		return fabs(f) <= 0.00001f;
	} 
	 
	inline float qDegreesToRadians(float degrees)
	{
		return degrees * float(M_PI / 180);
	}

	struct Line2D {
		coord_t ptA;
		coord_t ptB;
	};

	struct Line3D {
		Point3 ptA;
		Point3 ptB;
	};


	class QVector3D
	{
	public:
		QVector3D() : v{ 0.0f, 0.0f, 0.0f } {}
		QVector3D(float xpos, float ypos, float zpos) : v{ xpos, ypos, zpos } {}
		QVector3D(Point3 pt3d) : v{ (float)pt3d.x, (float)pt3d.y, (float)pt3d.z } {}

		float x() const;
		float y() const;
		float z() const;
		void normalize();
		QVector3D normalized() const;
		float lengthSquared() const;

		MeshVertex toMeshVertex() const;
		Point3 toPoint3() const;

		float &operator[](int i);
		float operator[](int i) const;

		static float dotProduct(const QVector3D& v1, const QVector3D& v2); //In Qt 6 convert to inline and constexpr
		static QVector3D crossProduct(const QVector3D& v1, const QVector3D& v2); //in Qt 6 convert to inline and constexpr

		friend inline const QVector3D operator+(const QVector3D &v1, const QVector3D &v2);
		friend inline const QVector3D operator/(const QVector3D &vector, float divisor);
		friend inline const QVector3D operator/(const QVector3D &vector, const QVector3D &divisor);
		friend inline const QVector3D operator-(const QVector3D &v1, const QVector3D &v2);
		friend inline const QVector3D operator-(const QVector3D &vector);
		friend const QVector3D operator*(const QVector3D &v1, const QVector3D& v2);
		friend inline const QVector3D operator*(float factor, const QVector3D &vector);
		friend inline const QVector3D operator*(const QVector3D &vector, float factor);


	private:
		float v[3];

	}; 


	inline float &QVector3D::operator[](int i)
	{
		//_ASSERT(uint(i) < 3u);
		return v[i]; 
	}

	inline float QVector3D::operator[](int i) const
	{
		//Q_ASSERT(uint(i) < 3u);
		return v[i];
	}
	inline const QVector3D operator+(const QVector3D &v1, const QVector3D &v2)
	{
		return QVector3D(v1.v[0] + v2.v[0], v1.v[1] + v2.v[1], v1.v[2] + v2.v[2]);
	}

	inline const QVector3D operator/(const QVector3D &vector, float divisor)
	{
		return QVector3D(vector.v[0] / divisor, vector.v[1] / divisor, vector.v[2] / divisor);
	}
	inline const QVector3D operator/(const QVector3D &vector, const QVector3D &divisor)
	{
		return QVector3D(vector.v[0] / divisor.v[0], vector.v[1] / divisor.v[1], vector.v[2] / divisor.v[2]);
	}
	inline const QVector3D operator-(const QVector3D &v1, const QVector3D &v2)
	{
		return QVector3D(v1.v[0] - v2.v[0], v1.v[1] - v2.v[1], v1.v[2] - v2.v[2]);
	}

	inline const QVector3D operator*(const QVector3D &v1, const QVector3D& v2)
	{
		return QVector3D(v1.v[0] * v2.v[0], v1.v[1] * v2.v[1], v1.v[2] * v2.v[2]);
	}

	inline const QVector3D operator*(float factor, const QVector3D &vector)
	{
		return QVector3D(vector.v[0] * factor, vector.v[1] * factor, vector.v[2] * factor);
	}

	inline const QVector3D operator*(const QVector3D &vector, float factor)
	{
		return QVector3D(vector.v[0] * factor, vector.v[1] * factor, vector.v[2] * factor);
	}
	inline const QVector3D operator-(const QVector3D &vector)
	{
		return QVector3D(-vector.v[0], -vector.v[1], -vector.v[2]);
	}
	inline float QVector3D::x() const { return v[0]; }
	inline float QVector3D::y() const { return v[1]; }
	inline float QVector3D::z() const { return v[2]; }



	class QQuaternion
	{
	public:
		QQuaternion();
		QQuaternion(float scalar, float xpos, float ypos, float zpos);
		QQuaternion(float scalar, const QVector3D& vector);
		QVector3D rotatedVector(const QVector3D& vector) const;
		QQuaternion conjugated() const;
		QQuaternion normalized() const;
		void normalize();
		QVector3D vector() const;

		friend inline const QQuaternion operator*(const QQuaternion &q1, const QQuaternion& q2);
		friend inline const QQuaternion operator/(const QQuaternion &quaternion, float divisor);
		static QQuaternion rotationTo(const QVector3D &from, const QVector3D &to);
		static QQuaternion fromEulerAngles(float pitch, float yaw, float roll);
		static inline QQuaternion fromEulerAngles(const QVector3D &eulerAngles);
	private:
		float wp, xp, yp, zp;

	};

	AABB3D getMeshBound(const Mesh& meshs);
	AABB3D getMeshsBound(const std::vector<Mesh>& meshs);


	void doMeshRotate(std::vector<Mesh>& meshs, const QQuaternion& q, Line3D* lineA = nullptr, Line3D* lineB = nullptr);


	void doMeshToPlat(std::vector<Mesh>& meshs, AABB3D preAabb, Line3D* lineA = nullptr, Line3D* lineB = nullptr);


	inline QQuaternion::QQuaternion(float aScalar, float xpos, float ypos, float zpos) : wp(aScalar), xp(xpos), yp(ypos), zp(zpos) {}
	 
	inline QQuaternion::QQuaternion(float aScalar, const QVector3D& aVector)
		: wp(aScalar), xp(aVector.x()), yp(aVector.y()), zp(aVector.z()) {} 

	inline const QQuaternion operator*(const QQuaternion &q1, const QQuaternion& q2)
	{
		float yy = (q1.wp - q1.yp) * (q2.wp + q2.zp);
		float zz = (q1.wp + q1.yp) * (q2.wp - q2.zp);
		float ww = (q1.zp + q1.xp) * (q2.xp + q2.yp);
		float xx = ww + yy + zz;
		float qq = 0.5f * (xx + (q1.zp - q1.xp) * (q2.xp - q2.yp));

		float w = qq - ww + (q1.zp - q1.yp) * (q2.yp - q2.zp);
		float x = qq - xx + (q1.xp + q1.wp) * (q2.xp + q2.wp);
		float y = qq - yy + (q1.wp - q1.xp) * (q2.yp + q2.zp);
		float z = qq - zz + (q1.zp + q1.yp) * (q2.wp - q2.xp);

		return QQuaternion(w, x, y, z);
	}

	inline const QQuaternion operator/(const QQuaternion &quaternion, float divisor)
	{
		return QQuaternion(quaternion.wp / divisor, quaternion.xp / divisor, quaternion.yp / divisor, quaternion.zp / divisor);
	}

	inline QQuaternion QQuaternion::conjugated() const
	{
		return QQuaternion(wp, -xp, -yp, -zp);
	}

	inline QVector3D QQuaternion::vector() const
	{
		return QVector3D(xp, yp, zp);
	}

	inline QQuaternion QQuaternion::fromEulerAngles(const QVector3D &eulerAngles)
	{
		return QQuaternion::fromEulerAngles(eulerAngles.x(), eulerAngles.y(), eulerAngles.z());
	}


	QVector3D calcRayHitPlane(const QVector3D& rayO, const QVector3D& rayDir, const QVector3D& planeO, const QVector3D& planeDir);


	struct Plane
	{
		Plane() {}
		Plane(const QVector3D& _pos, const QVector3D& _nor) {
			origin = _pos;
			normal = _nor;
		}

		QVector3D origin;
		QVector3D normal;

		bool intersection(QVector3D& out, const QVector3D& la, const QVector3D& lb) const
		{
			float a = QVector3D::dotProduct(origin - la, normal); // 计算out需要: p.origin - la
			float b = QVector3D::dotProduct(origin - lb, normal);

			// 线段的两个端点在平面的同一边，或者在平面上，没有交点，返回。
			if (a > 0 && b > 0 || a < 0 && b < 0 || qFuzzyIsNull(a) && qFuzzyIsNull(b)) {
				return false;
			}
			else if (qFuzzyIsNull(a) && b > 0 || a > 0 && qFuzzyIsNull(b)) {
				return false;
			}
			else if (qFuzzyIsNull(a)) {
				out = la;
				return true;
			}
			else if (qFuzzyIsNull(b)) {
				out = lb;
				return true;
			}

			QVector3D l = lb - la;
			float c = QVector3D::dotProduct(l, normal);

			if (qFuzzyIsNull(c)) { // 直线与平面平行
				return false;
			}

			out = la + ((a / c) * l);
			return true;
		}

		bool intersectionRay(QVector3D& out, const QVector3D& rayOri, const QVector3D& rayDir) const
		{
			float denom = QVector3D::dotProduct(normal, rayDir);
			if (false == qFuzzyIsNull(denom)) // your favorite epsilon
			{
				float t = QVector3D::dotProduct((origin - rayOri), normal) / denom;
				if (t > 0 || qFuzzyIsNull(t)) {
					out = rayOri + rayDir * t;
					return true; // you might want to allow an epsilon here too
				}
			}
			return false;
		}
	};
}



#endif // ANYCUBIC_ROLL_H


// AnyCubic : Hanson : End